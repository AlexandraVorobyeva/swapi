
export const entitiesConfig = {
  people: {
    titleProperty: 'name',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {homeworld: 'planets'},
    fieldsToBeArrayOfUrls: {species: 'species', films: 'films', vehicles: 'vehicles', starships: 'starships'}
  },
  films: {
    titleProperty: 'title',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {},
    fieldsToBeArrayOfUrls: {characters: 'people', planets: 'planets', starships: 'starships', vehicles: 'vehicles', species: 'species'}
  },
  starships: {
    titleProperty: 'name',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {},
    fieldsToBeArrayOfUrls: {films: 'films', pilots: 'people'}
  },
  species: {
    titleProperty: 'name',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {},
    fieldsToBeArrayOfUrls: {}
  },
  vehicles: {
    titleProperty: 'name',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {},
    fieldsToBeArrayOfUrls: {}
  },
  planets: {
    titleProperty: 'name',
    fieldsToExclude: {url: true, created: true, edited: true},
    fieldsToBeUrl: {},
    fieldsToBeArrayOfUrls: {}
  },
};
