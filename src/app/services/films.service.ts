import {Injectable} from '@angular/core';
import {EntityService} from './entity.service';
import {entitiesConfig} from '../entitiesData';

@Injectable({
  providedIn: 'root'
})
export class FilmsService extends EntityService {
  mainProperties = ['episode_id', 'director'];
  entityType = 'films';
  titleProperty = entitiesConfig.films.titleProperty;
}
