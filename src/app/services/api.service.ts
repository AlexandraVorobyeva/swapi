import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {entitiesConfig} from '../entitiesData';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly baseURL = 'https://swapi.co/api/';

  constructor(private readonly http: HttpClient) {
  }

  getList(entityType, page) {
    return this.http.get(`${this.baseURL}${entityType}/?page=${page}`);
  }

  getListWithSearch(entityType, searchTerm, page) {
    return this.http.get(`${this.baseURL}${entityType}/?search=${searchTerm}&page=${page}`);
  }

  getEntity(entityType, id) {
    return this.http.get(`${this.baseURL}${entityType}/${id}/`);
  }

  getTitleByUrl(url, entityType) {
    return this.http.get(url)
      .pipe(map(res => res[entitiesConfig[entityType].titleProperty]));
  }
}
