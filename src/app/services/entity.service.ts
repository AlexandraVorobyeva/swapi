import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export abstract class EntityService {

  public readonly titleProperty: string;
  public readonly mainProperties = [];
  protected readonly entityType;

  constructor(private api: ApiService, private router: Router) {
  }

  public getData(searchTerm, page) {
    if (searchTerm) {
      return this.api.getListWithSearch(this.entityType, searchTerm, page);
    } else {
      return this.api.getList(this.entityType, page);
    }
  }

  public openPopup(url) {
    const match = url.match(new RegExp(`${this.entityType}\/(\\d+)\/`));
    const id = match && match[1];
    this.router.navigate([{outlets: {popup: 'details/' + this.entityType + '/' + id}}]);
  }
}
