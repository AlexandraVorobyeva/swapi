import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject} from 'rxjs';
import {EntityService} from './entity.service';
import {entitiesConfig} from '../entitiesData';

@Injectable({
  providedIn: 'root'
})
export class StarshipsService extends EntityService {
  titleProperty = entitiesConfig.starships.titleProperty;
  mainProperties = ['model', 'manufacturer', 'cost_in_credits'];
  entityType = 'starships';
}
