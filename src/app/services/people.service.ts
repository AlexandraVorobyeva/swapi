import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject} from 'rxjs';
import {EntityService} from './entity.service';
import {entitiesConfig} from '../entitiesData';

@Injectable({
  providedIn: 'root'
})
export class PeopleService extends EntityService {
  titleProperty = entitiesConfig.people.titleProperty;
  mainProperties = ['height', 'mass', 'hair_color', 'skin_color'];
  entityType = 'people';
}
