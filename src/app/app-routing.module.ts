import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FilmsComponent} from './components/films/films.component';
import {PeopleComponent} from './components/people/people.component';
import {StarshipsComponent} from './components/starships/starships.component';
import {DetailsComponent} from './components/details/details.component';

const routes: Routes = [
  {path: 'details/:entityType/:id', component: DetailsComponent, outlet: 'popup'},
  {
    path: '',
    children: [
      {path: 'films', component: FilmsComponent},
      {path: 'people', component: PeopleComponent},
      {path: 'starships', component: StarshipsComponent},
    ]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
