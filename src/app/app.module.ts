import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { PeopleComponent } from './components/people/people.component';
import { FilmsComponent } from './components/films/films.component';
import { StarshipsComponent } from './components/starships/starships.component';
import { DetailsComponent } from './components/details/details.component';
import { TableComponent } from './components/table/table.component';
import {HttpClientModule} from '@angular/common/http';
import {SearchInputComponent} from './components/search-input/search-input.component';
import {FormsModule} from '@angular/forms';
import {EntityService} from './services/entity.service';
import { EntityComponent } from './components/entity/entity.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PeopleComponent,
    FilmsComponent,
    StarshipsComponent,
    DetailsComponent,
    TableComponent,
    SearchInputComponent,
    EntityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
