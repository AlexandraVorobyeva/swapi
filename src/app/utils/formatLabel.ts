export function formatLabel(label: string): string {
  const formatted = label.replace(new RegExp('_', 'g'), ' ');
  return formatted.substr(0, 1).toUpperCase() + formatted.substr(1);
}
