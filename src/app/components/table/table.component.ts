import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable, ObservableInput} from 'rxjs';
import {Router} from '@angular/router';
import {formatLabel} from '../../utils/formatLabel';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input()
  data$: Observable<any>;

  @Input()
  columns: string[];

  @Input()
  currentPage = 1;

  @Output()
  openDetails = new EventEmitter();

  @Output()
  pageChanged = new EventEmitter();

  rowsOnPage = 10;

  formatLabel = formatLabel;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  prevPage() {
    this.pageChanged.emit(this.currentPage - 1);
  }

  nextPage() {
    this.pageChanged.emit(this.currentPage + 1);
  }

  openPopup(id) {
    this.openDetails.next(id);
  }

}
