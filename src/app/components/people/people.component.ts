import {Component, OnInit} from '@angular/core';
import {PeopleService} from '../../services/people.service';
import {EntityComponent} from '../entity/entity.component';

@Component({
  selector: 'app-people',
  templateUrl: '../entity/entity.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent extends EntityComponent {

  constructor(public entityService: PeopleService) {
    super(entityService);
  }

}
