import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ApiService} from '../../services/api.service';
import {entitiesConfig} from '../../entitiesData';
import {formatLabel} from '../../utils/formatLabel';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {

  public title;
  public data;

  private destroyed$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public api: ApiService) {
  }

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.destroyed$)).subscribe(q => {
      this.api.getEntity(q.entityType, q.id)
        .subscribe(res => {
          const config = entitiesConfig[q.entityType];
          const { titleProperty, fieldsToExclude, fieldsToBeUrl, fieldsToBeArrayOfUrls } = config;
          this.title = res[titleProperty];
          fieldsToExclude[titleProperty] = true;
          this.data = [];

          for (const key in res) {
            if (fieldsToExclude[key]) {
              continue;
            }
            if (res[key]) {
              const label = formatLabel(key);
              if (fieldsToBeUrl[key]) {
                const value = this.api.getTitleByUrl(res[key], fieldsToBeUrl[key]);
                this.data.push({label, value, isUrl: true, isArrayOfUrls: false});
              } else if (fieldsToBeArrayOfUrls[key]) {
                const value = res[key].map(el => this.api.getTitleByUrl(el, fieldsToBeArrayOfUrls[key]));
                this.data.push({label, value, isUrl: false, isArrayOfUrls: true});
              } else {
                this.data.push({label, value: res[key], isUrl: false, isArrayOfUrls: false});
              }
            }
          }
        });
    });
  }

  close() {
    this.router.navigate([{outlets: {popup: null}}]);
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

}
