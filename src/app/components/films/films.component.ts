import {Component, OnInit} from '@angular/core';
import {FilmsService} from '../../services/films.service';
import {PeopleService} from '../../services/people.service';
import {EntityComponent} from '../entity/entity.component';

@Component({
  selector: 'app-films',
  templateUrl: '../entity/entity.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent extends EntityComponent {

  constructor(public entityService: FilmsService) {
    super(entityService);
  }

}
