import {Component, OnInit} from '@angular/core';
import {PeopleService} from '../../services/people.service';
import {EntityService} from '../../services/entity.service';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.scss']
})
export class EntityComponent implements OnInit {

  public data$ = null;
  public columns = [];
  public page = 1;
  public searchTerm = '';

  constructor(public entityService: EntityService) {
  }

  ngOnInit() {
    this.data$ = this.entityService.getData(this.searchTerm, this.page);
    this.columns = [this.entityService.titleProperty, ...this.entityService.mainProperties];
  }

  getData() {
    this.data$ = this.entityService.getData(this.searchTerm, this.page);
  }

  pageChanged(page) {
    this.page = page;
    this.getData();
  }

  searchFor(searchTerm) {
    this.searchTerm = searchTerm;
    this.page = 1;
    this.getData();
  }

  openPopup(url) {
    this.entityService.openPopup(url);
  }

}
