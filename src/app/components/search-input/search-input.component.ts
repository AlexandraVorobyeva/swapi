import {AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subject, fromEvent} from 'rxjs';
import {distinctUntilChanged, debounceTime, takeUntil, take} from 'rxjs/operators';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements AfterViewInit, OnDestroy, OnInit {
  public inputValue = '';

  /** Поле поиска */
  @ViewChild('searchTerm', {static: false}) searchTermElRef: ElementRef;

  @Output()
  searchTermChanged = new EventEmitter<string>();

  /** Подписки живут пока не... */
  destroyed: Subject<void> = new Subject<void>();

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // подписывается на keyup поля поиска
    // нужно тут, чтобы можно было навешать debounceTime и distinctUntilChanged
    fromEvent(this.searchTermElRef.nativeElement, 'keyup')
      .pipe(
        takeUntil(this.destroyed),
        debounceTime(300),
        distinctUntilChanged())
      .subscribe((keyboardEvent: any) => {
        this.searchTermChanged.next(keyboardEvent.target.value);
      });
  }

  /**
   * очистить поле, сбросить поиск
   */
  clear() {
    this.inputValue = '';
    this.searchTermChanged.next('');
  }

  ngOnDestroy() {
    this.clear();
    this.destroyed.next();
    this.destroyed.complete();
  }

}
