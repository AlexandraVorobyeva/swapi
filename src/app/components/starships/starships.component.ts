import {Component, OnInit} from '@angular/core';
import {EntityComponent} from '../entity/entity.component';
import {StarshipsService} from '../../services/starships.service';

@Component({
  selector: 'app-starships',
  templateUrl: '../entity/entity.component.html',
  styleUrls: []
})
export class StarshipsComponent extends EntityComponent {

  constructor(public starshipsService: StarshipsService) {
    super(starshipsService);
  }

}
